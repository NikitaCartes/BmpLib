#include "Picture.hpp"

#include <fstream>

Picture::Picture(const Picture& image, const uint16_t bitCount)
  : Picture(image.getHeight(), image.getWidth(), bitCount)
{
  for (uint32_t i = 0; i < image.getHeight(); i++)
  {
    for (uint32_t j = 0; j < image.getWidth(); j++)
    {
      setPixelAt(i, j, image.getPixelAt(i, j));
    }
  }
}

Picture::Picture(const std::experimental::filesystem::path& path)
{
  std::ifstream fin(path, std::ios::binary);
  pathToFile_ = path;
  {
    fin.read(reinterpret_cast<char*>(&bmpHeader_.fileHeader.signature), size_t(2));
    if (bmpHeader_.fileHeader.signature != 19778)
    {
      throw "This file isn't BMP";
    }
    fin.read(reinterpret_cast<char*>(&bmpHeader_.fileHeader.fileSize), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.fileHeader.reserved), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.fileHeader.dataOffset), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.infoHeaderSize), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.width), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.height), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.planes), size_t(2));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.bitCount), size_t(2));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.compression), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.imageSize), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.xPixelsPerM), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.yPixelsPerM), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.colorsUsed), size_t(4));
    fin.read(reinterpret_cast<char*>(&bmpHeader_.infoHeader.colorsImportant), size_t(4));
  }
  bitMap_ = std::vector<std::vector<Pixel>>(getHeight(), std::vector<Pixel>(getWidth()));
  rawData_ = std::vector<uint8_t>();
  switch (bmpHeader_.infoHeader.bitCount)
  {
  case 24:
  {
    palette_ = std::vector<Pixel>();
    fin.seekg(bmpHeader_.fileHeader.dataOffset);
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j++)
      {
        setPixelAt(i, j, { fin.get() , fin.get() , fin.get() });
      }
      if ((getWidth() * 3) % 4 != 0)
      {
        uint32_t trash;
        fin.read(reinterpret_cast<char*>(&trash), size_t(4 - ((getWidth() * 3) % 4)));
      }
    }
    break;
  }
  case 16:
  {
    palette_ = std::vector<Pixel>();
    fin.seekg(bmpHeader_.fileHeader.dataOffset);
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j++)
      {
        uint16_t pixel;
        fin.read(reinterpret_cast<char*>(&pixel), size_t(2));
        setPixelAt(i, j, { (pixel & 0b000000000011111) << 3, (pixel & 0b000001111100000) >> 2 ,(pixel & 0b111110000000000) >> 7 });
      }
      if ((getWidth() * 2) % 4 != 0)
      {
        uint32_t trash;
        fin.read(reinterpret_cast<char*>(&trash), size_t(4 - ((getWidth() * 2) % 4)));
      }
    }
    break;
  }
  case 8:
  {
    palette_ = std::vector<Pixel>(bmpHeader_.infoHeader.colorsUsed == 0 ? 256 : bmpHeader_.infoHeader.colorsUsed);
    for (size_t i = 0; i < palette_.size(); i++)
    {
      palette_[i] = { fin.get(),  fin.get(),  fin.get() };
      fin.get();
    }
    fin.seekg(bmpHeader_.fileHeader.dataOffset);
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j++)
      {
        setPixelAt(i, j, palette_[fin.get()]);
      }
      if ((getWidth() * 1) % 4 != 0)
      {
        uint32_t trash;
        fin.read(reinterpret_cast<char*>(&trash), size_t(4 - (getWidth() % 4)));
      }
    }
    break;
  }
  case 4:
  {
    palette_ = std::vector<Pixel>(bmpHeader_.infoHeader.colorsUsed == 0 ? 16 : bmpHeader_.infoHeader.colorsUsed);
    for (size_t i = 0; i < palette_.size(); i++)
    {
      palette_[i] = { fin.get(),  fin.get(),  fin.get() };
      fin.get();
    }
    fin.seekg(bmpHeader_.fileHeader.dataOffset);
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j += 2)
      {
        int t = fin.get();
        setPixelAt(i, j, palette_[(t & 0b11110000) >> 4]);
        if (j + 1 < getWidth())
        {
          setPixelAt(i, j + 1, palette_[t & 0b00001111]);
        }
      }
      if (std::lround(std::ceil(getWidth() / 2.0)) % 4 != 0)
      {
        uint32_t trash;
        fin.read(reinterpret_cast<char*>(&trash), size_t(4 - (std::lround(std::ceil(getWidth() / 2.0)) % 4)));
      }
    }
    break;
  }
  default:
  {
    throw "Illegal bitCount";
    break;
  }
  }
  coefficients_red_ = 0.2126;
  coefficients_green_ = 0.7152;
  coefficients_blue_ = 0.0722;
  return;
}

Picture::Picture(const uint32_t height, const uint32_t width, const uint16_t bitCount)
{
  bmpHeader_.fileHeader.signature = 19778;
  bmpHeader_.fileHeader.reserved = 0;

  switch (bitCount)
  {
  case 24:
  {
    bmpHeader_.fileHeader.fileSize = 54 + (3 * width + ((width * (-3)) % 4)) * height;
    bmpHeader_.fileHeader.dataOffset = 54;
    break;
  }
  case 16:
  {
    bmpHeader_.fileHeader.fileSize = 54 + (2 * width + ((width * (-2)) % 4)) * height;
    bmpHeader_.fileHeader.dataOffset = 54;
    break;
  }
  case 8:
  {
    bmpHeader_.fileHeader.fileSize = 54 + 1024 + (width + ((width * (-1)) % 4)) * height;
    bmpHeader_.fileHeader.dataOffset = 54 + 1024;
    break;
  }
  case 4:
  {
    bmpHeader_.fileHeader.fileSize = 54 + 64 + (width / 2 + ((width * (-1) / 2) % 4)) * height;
    bmpHeader_.fileHeader.dataOffset = 54 + 64;
    break;
  }
  default:
  {
    throw "Illegal bitCount";
    break;
  }
  }

  bmpHeader_.infoHeader.infoHeaderSize = 40;
  bmpHeader_.infoHeader.width = width;
  bmpHeader_.infoHeader.height = height;
  bmpHeader_.infoHeader.planes = 1;
  bmpHeader_.infoHeader.bitCount = bitCount;
  bmpHeader_.infoHeader.compression = 0;
  bmpHeader_.infoHeader.imageSize = 0;
  bmpHeader_.infoHeader.xPixelsPerM = 0;
  bmpHeader_.infoHeader.yPixelsPerM = 0;
  bmpHeader_.infoHeader.colorsUsed = 0;
  bmpHeader_.infoHeader.colorsImportant = 0;

  bitMap_ = std::vector<std::vector<Pixel>>(getHeight(), std::vector<Pixel>(getWidth()));
  palette_ = std::vector<Pixel>();
  coefficients_red_ = 0.2126;
  coefficients_green_ = 0.7152;
  coefficients_blue_ = 0.0722;
  return;
}

void Picture::writeToFile(const std::experimental::filesystem::path& path)
{
  std::ofstream fout(path, std::ios::binary);
  {
    fout.write(reinterpret_cast<char*>(&bmpHeader_.fileHeader.signature), size_t(2));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.fileHeader.fileSize), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.fileHeader.reserved), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.fileHeader.dataOffset), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.infoHeaderSize), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.width), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.height), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.planes), size_t(2));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.bitCount), size_t(2));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.compression), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.imageSize), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.xPixelsPerM), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.yPixelsPerM), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.colorsUsed), size_t(4));
    fout.write(reinterpret_cast<char*>(&bmpHeader_.infoHeader.colorsImportant), size_t(4));
  }
  if (rawData_.size() < bmpHeader_.fileHeader.fileSize - bmpHeader_.fileHeader.dataOffset)
  {
    prepareToWrite();
  }
  for (auto t : rawData_)
  {
    fout.put(t);
  }
  return;
}

void Picture::prepareToWrite()
{
  if (palette_.size() < 1)
  {
    if (bmpHeader_.infoHeader.bitCount == 8)
    {
      createPalette(256, true);
    }
    if (bmpHeader_.infoHeader.bitCount == 4)
    {
      createPalette(16, true);
    }
  }
  if (spreadMatrix_.size() < 1)
  {
    std::vector<std::vector<double>> temp = std::vector<std::vector<double>>(3, std::vector<double>(5, 0));
    temp[0][3] = 5.0 / 32;
    temp[0][4] = 3.0 / 32;
    temp[1][0] = 2.0 / 32;
    temp[1][1] = 4.0 / 32;
    temp[1][2] = 5.0 / 32;
    temp[1][3] = 4.0 / 32;
    temp[1][4] = 2.0 / 32;
    temp[2][1] = 2.0 / 32;
    temp[2][2] = 3.0 / 32;
    temp[2][3] = 2.0 / 32;
    setSpreadMatrix(temp, 2);
  }
  switch (bmpHeader_.infoHeader.bitCount)
  {
  case 24:
  {
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j++)
      {
        putPixelInData(getPixelAt(i, j));
      }
      if ((getWidth() * 3) % 4 != 0)
      {
        putPixelInData(0, size_t(4 - ((getWidth() * 3) % 4)));
      }
    }
    break;
  }
  case 16:
  {
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j++)
      {
        Pixel pixel = getPixelAt(i, j);
        Pixel floorPixel = { pixel.red & 0b11111000, pixel.green & 0b11111000, pixel.blue & 0b11111000 };
        Pixel ceilPixel = { floorPixel.red + 8, floorPixel.green + 8, floorPixel.blue + 8 };
        if (std::abs(ceilPixel.red - pixel.red) < (pixel.red - floorPixel.red))
        {
          pixel.red = ceilPixel.red;
        }
        else
        {
          pixel.red = floorPixel.red;
        }
        if (std::abs(ceilPixel.green - pixel.green) < (pixel.green - floorPixel.green))
        {
          pixel.green = ceilPixel.green;
        }
        else
        {
          pixel.green = floorPixel.green;
        }
        if (std::abs(ceilPixel.blue - pixel.blue) < (pixel.blue - floorPixel.blue))
        {
          pixel.blue = ceilPixel.blue;
        }
        else
        {
          pixel.blue = floorPixel.blue;
        }
        setPixelAt(i, j, pixel);
        putPixelInData(static_cast<uint16_t>((pixel.blue << 7) | (pixel.green << 2) | (pixel.red >> 3)));
      }
      if ((getWidth() * 2) % 4 != 0)
      {
        putPixelInData(0, size_t(4 - ((getWidth() * 2) % 4)));
      }
    }
    break;
  }
  case 8:
  {
    for (uint32_t i = 0; i < 256; i++)
    {
      Pixel pixel = palette_[i];
      putPixelInData(pixel);
      putPixelInData(0);
    }
    std::vector<std::vector<unsignedPixel>> errors = std::vector<std::vector<unsignedPixel>>(getHeight(), std::vector<unsignedPixel>(getWidth(), { 0,0,0 }));
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j++)
      {
        Pixel pixel = getPixelAt(i, j);
        pixel = pixel + errors[i][j];
        int t = findNearestInPalette(pixel);
        spreadError(errors, i, j, palette_[t], pixel);
        setPixelAt(i, j, palette_[t]);
        putPixelInData(t);
      }
      if ((getWidth() * 1) % 4 != 0)
      {
        putPixelInData(0, size_t(4 - (getWidth() % 4)));
      }
    }
    break;
  }
  case 4:
  {
    for (uint32_t i = 0; i < 16; i++)
    {
      Pixel pixel = palette_[i];
      putPixelInData(pixel);
      putPixelInData(0);
    }
    std::vector<std::vector<unsignedPixel>> errors = std::vector<std::vector<unsignedPixel>>(getHeight(), std::vector<unsignedPixel>(getWidth(), { 0,0,0 }));
    for (uint32_t i = 0; i < getHeight(); i++)
    {
      for (uint32_t j = 0; j < getWidth(); j += 2)
      {
        Pixel pixel = getPixelAt(i, j);
        pixel = pixel + errors[i][j];
        int t = findNearestInPalette(pixel);
        spreadError(errors, i, j, palette_[t], pixel);
        setPixelAt(i, j, palette_[t]);
        int t1 = (t & 0b1111) << 4;
        if (j + 1 < getWidth())
        {
          pixel = getPixelAt(i, j + 1);
          pixel = pixel + errors[i][j + 1];
          t = findNearestInPalette(pixel);
          spreadError(errors, i, j + 1, palette_[t], pixel);
          setPixelAt(i, j + 1, palette_[t]);
          t1 |= (t & 0b1111);
        }
        putPixelInData(t1);
      }
      if (std::lround(std::ceil(getWidth() / 2.0)) % 4 != 0)
      {
        putPixelInData(0, size_t(4 - (std::lround(std::ceil(getWidth() / 2.0)) % 4)));
      }
    }
    break;
  }
  default:
  {
    throw "Illegal bitCount";
    break;
  }
  }
  return;
}

int Picture::findNearestInPalette(const Pixel pixel) const
{
  double min = 0xffffffff;
  int min_i = 0;
  double delta = 0;
  int i = 0;
  for (auto t : palette_)
  {
    delta = coefficients_red_ * (t.red - pixel.red)*(t.red - pixel.red)
      + coefficients_green_ * (t.green - pixel.green)*(t.green - pixel.green)
      + coefficients_blue_ * (t.blue - pixel.blue)*(t.blue - pixel.blue);
    if (delta <= min)
    {
      min = delta;
      min_i = i;
    }
    i++;
  }
  return min_i;
}

void Picture::spreadError(std::vector<std::vector<unsignedPixel>>& errors, const uint32_t heigth, const uint32_t width, const Pixel existingPixel, const Pixel needPixel)
{
  const uint32_t heigthImage = getHeight();
  const uint32_t widthImage = getWidth();
  const size_t heigthMatrix = spreadMatrix_.size();
  const size_t widthMatrix = spreadMatrix_[0].size();
  unsignedPixel error{ needPixel.red - existingPixel.red, needPixel.green - existingPixel.green, needPixel.blue - existingPixel.blue };
  //#pragma omp parallel for
  for (size_t i = 0; i < heigthMatrix; i++)
  {
    //#pragma omp parallel for
    for (size_t j = 0; j < widthMatrix; j++)
    {
      const double koef = spreadMatrix_[i][j];
      if (koef != 0)
      {
        if (((heigth + i) < heigthImage) && ((width + j - spreadMatrixStartJ_) < widthImage))
        {
          errors[heigth + i][width + j - spreadMatrixStartJ_] += koef * (error);
        }
      }
    }
  }
}

cv::Mat Picture::convertToOpenCV() const
{
  cv::Mat tempImage(getHeight(), getWidth(), CV_8UC3);
  for (int y = 0; y < tempImage.rows; y++)
  {
    for (int x = 0; x < tempImage.cols; x++)
    {
      Pixel pixel = getPixelAt(getHeight() - y - 1, getWidth() - x - 1);
      cv::Vec3b pixelOpenCV{ pixel.red, pixel.green, pixel.blue };
      tempImage.at<cv::Vec3b>(cv::Point(getWidth() - x - 1, y)) = pixelOpenCV;
    }
  }
  return tempImage;
}

std::vector <Pixel> Picture::createPalette(const size_t nColour, bool var)
{
  std::vector <statsPixel> usedColours = getUsedColours();

  //-------------- Var_1
  if (var)
  {
    std::vector <std::vector <statsPixel>> palette_1 = std::vector<std::vector<statsPixel>>(nColour);
    palette_1[0] = usedColours;

    size_t k = 1;
    while (k < nColour)
    {
      for (size_t j = 0; j < k; j++)
      {
        Pixel pixel_max{ 0,0,0 };
        Pixel pixel_min{ 255, 255, 255 };
        for (auto i : palette_1[j])
        {
          if (i.pixel.red > pixel_max.red)
          {
            pixel_max.red = i.pixel.red;
          }
          if (i.pixel.green > pixel_max.green)
          {
            pixel_max.green = i.pixel.green;
          }
          if (i.pixel.blue > pixel_max.blue)
          {
            pixel_max.blue = i.pixel.blue;
          }
          if (i.pixel.red < pixel_min.red)
          {
            pixel_min.red = i.pixel.red;
          }
          if (i.pixel.green < pixel_min.green)
          {
            pixel_min.green = i.pixel.green;
          }
          if (i.pixel.blue < pixel_min.blue)
          {
            pixel_min.blue = i.pixel.blue;
          }
        }

        std::vector <statsPixel> temp1;
        std::vector <statsPixel> temp2;

        double red = pixel_max.red - pixel_min.red;
        double green = pixel_max.green - pixel_min.green;
        double blue = pixel_max.blue - pixel_min.blue;

        if ((red >= green) && (red >= blue))
        {
          red = (pixel_max.red + pixel_min.red) / 2;
          for (auto i : palette_1[j])
          {
            if (i.pixel.red > red)
            {
              temp1.push_back(i);
            }
            else
            {
              temp2.push_back(i);
            }
          }
        }
        else if ((green >= red) && (green >= blue))
        {
          green = (pixel_max.green + pixel_min.green) / 2;
          for (auto i : palette_1[j])
          {
            if (i.pixel.green > green)
            {
              temp1.push_back(i);
            }
            else
            {
              temp2.push_back(i);
            }
          }
        }
        else if ((blue >= red) && (blue >= green))
        {
          blue = (pixel_max.blue + pixel_min.blue) / 2;
          for (auto i : palette_1[j])
          {
            if (i.pixel.blue > blue)
            {
              temp1.push_back(i);
            }
            else
            {
              temp2.push_back(i);
            }
          }
        }

        palette_1[j] = temp1;
        palette_1[k + j] = temp2;
      }
      k *= 2;
    }

    std::vector <Pixel> temp0;
    for (size_t i = 0; i < nColour; i++)
    {
      double R = 0;
      double G = 0;
      double B = 0;
      uint64_t n = 0;
      for (auto k : palette_1[i])
      {
        R = R + k.pixel.red * k.nUsage;
        G = G + k.pixel.green * k.nUsage;
        B = B + k.pixel.blue * k.nUsage;
        n += k.nUsage;
      }
      temp0.push_back({ static_cast<uint8_t>(R / n), static_cast<uint8_t>(G / n), static_cast<uint8_t>(B / n) });
    }
    return temp0;
  }

  //-------------- Var_2
  else
  {
    std::vector <std::vector <statsPixel>> palette_1;
    palette_1.push_back(usedColours);

    while (palette_1.size() < nColour)
    {
      size_t size = 0;
      size_t j = 0;
      for (size_t i = 0; i < palette_1.size(); i++)
      {
        if (palette_1[i].size() >= size)
        {
          j = i;
          size = palette_1[i].size();
        }
      }

      double red = 0;
      double green = 0;
      double blue = 0;
      uint64_t n = 0;

      for (auto i : palette_1[j])
      {
        red += i.pixel.red * i.nUsage;
        green += i.pixel.green * i.nUsage;
        blue += i.pixel.blue * i.nUsage;
        n += i.nUsage;
      }

      std::vector <statsPixel> temp1;
      std::vector <statsPixel> temp2;

      red = red / n;
      green = green / n;
      blue = blue / n;

      if ((red >= green) && (red >= blue))
      {
        for (auto i : palette_1[j])
        {
          if (i.pixel.red > red)
          {
            temp1.push_back(i);
          }
          else
          {
            temp2.push_back(i);
          }
        }
      }
      else if ((green >= red) && (green >= blue))
      {
        for (auto i : palette_1[j])
        {
          if (i.pixel.green > green)
          {
            temp1.push_back(i);
          }
          else
          {
            temp2.push_back(i);
          }
        }
      }
      else if ((blue >= red) && (blue >= green))
      {
        for (auto i : palette_1[j])
        {
          if (i.pixel.blue > blue)
          {
            temp1.push_back(i);
          }
          else
          {
            temp2.push_back(i);
          }
        }
      }

      palette_1[j] = temp1;
      palette_1.push_back(temp2);
    }

    std::vector <Pixel> temp0;
    for (size_t i = 0; i < nColour; i++)
    {
      double R = 0;
      double G = 0;
      double B = 0;
      uint64_t n = 0;
      for (auto k : palette_1[i])
      {
        R = R + k.pixel.red * k.nUsage;
        G = G + k.pixel.green * k.nUsage;
        B = B + k.pixel.blue * k.nUsage;
        n += k.nUsage;
      }
      temp0.push_back({ static_cast<uint8_t>(R / n), static_cast<uint8_t>(G / n), static_cast<uint8_t>(B / n) });
    }
    return temp0;
  }
}

cv::Mat Picture::createPaletteOpenCV(const int nColour) const
{
  cv::Mat image = convertToOpenCV();
  image = image.reshape(1, image.rows*image.cols);
  cv::Mat bestLabels, centers;
  int attempts = 1;
  int clusts = nColour;
  double eps = 0.001;
  image.convertTo(image, CV_32FC3, 1.0 / 255.0);
  kmeans(image, clusts, bestLabels,
    cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, attempts, eps),
    attempts, cv::KMEANS_PP_CENTERS, centers);
  return centers;
}

std::vector <statsPixel> Picture::getUsedColours()
{
  usedColours_ = std::vector <statsPixel>();
  std::vector<std::vector <statsPixel>> tempUsedColours(256, std::vector <statsPixel>());
  //for (uint32_t i = 0; i < getHeight(); i++)
  for (auto& i : bitMap_)
  {
    //for (uint32_t j = 0; j < getWidth(); j++)
    for (auto& pixel : i)
    {
      //Pixel pixel = getPixelAt(i, j);
      bool isExist = false;
      for (auto& k : tempUsedColours[pixel.red])
      {
        if (k.pixel == pixel)
        {
          k.nUsage++;
          isExist = true;
          break;
        }
      }
      if (!isExist)
      {
        tempUsedColours[pixel.red].push_back({ pixel, 1 });
      }
    }
  }
  for (auto i : tempUsedColours)
  {
    usedColours_.insert(usedColours_.cend(), i.cbegin(), i.cend());
  }
  return usedColours_;
}

uint32_t Picture::getWidth() const
{
  return bmpHeader_.infoHeader.width;
}

uint32_t Picture::getHeight() const
{
  return bmpHeader_.infoHeader.height;
}

uint16_t Picture::getBitCount() const
{
  return bmpHeader_.infoHeader.bitCount;
}

void Picture::setPixelAt(const uint32_t heigth, const uint32_t width, const Pixel pixel)
{
  bitMap_[heigth][width] = pixel;
  return;
}

Pixel Picture::getPixelAt(const uint32_t heigth, const uint32_t width) const
{
  return bitMap_[heigth][width];
}

void Picture::setPalette(const std::vector <Pixel>& palette)
{
  palette_ = palette;
  return;
}

void Picture::setPalette(const cv::Mat paletteOpenCV)
{
  //CvMat color(paletteOpenCV);
  std::vector <Pixel> palette;
  for (int i = 0; i < paletteOpenCV.rows; i++)
  {
    palette.push_back({ 255 * paletteOpenCV.at<float>(i, 0), 255 * paletteOpenCV.at<float>(i, 1), 255 * paletteOpenCV.at<float>(i, 2) });
  }
  setPalette(palette);
  return;
}

void Picture::setSpreadMatrix(const std::vector<std::vector<double>> matrix, const uint8_t spreadMatrixStartJ)
{
  spreadMatrix_ = matrix;
  spreadMatrixStartJ_ = spreadMatrixStartJ;
}

std::vector <Pixel> Picture::getPalette() const
{
  return palette_;
}

std::experimental::filesystem::path Picture::getPath() const
{
  return pathToFile_;
}

void Picture::putPixelInData(const Pixel pixel)
{
  rawData_.push_back(pixel.red);
  rawData_.push_back(pixel.green);
  rawData_.push_back(pixel.blue);
  return;
}

void Picture::putPixelInData(const uint16_t pixel)
{
  rawData_.push_back(static_cast<uint8_t>(pixel & 0b11111111));
  rawData_.push_back(static_cast<uint8_t>((pixel & 0b1111111100000000) >> 8));
  return;
}

void Picture::putPixelInData(const uint8_t pixel, const uint8_t count)
{
  rawData_.insert(rawData_.cend(), pixel, count);
  return;
}

void Picture::putPixelInData(const int pixel)
{
  rawData_.push_back(static_cast<uint8_t>(pixel & 0b11111111));
  return;
}