#include "BaseTypes.hpp"

#include <cmath>

Pixel::Pixel()
{
  this->red = 0;
  this->green = 0;
  this->blue = 0;
}

Pixel::Pixel(const int red, const int green, const int blue)
{
  this->red = static_cast<uint8_t>(red);
  this->green = static_cast<uint8_t>(green);
  this->blue = static_cast<uint8_t>(blue);
}

Pixel::Pixel(const uint8_t red, const uint8_t green, const uint8_t blue)
{
  this->red = red;
  this->green = green;
  this->blue = blue;
}

Pixel::Pixel(const float red, const float green, const float blue)
{
  this->red = static_cast<uint8_t>(std::lround(red));
  this->green = static_cast<uint8_t>(std::lround(green));
  this->blue = static_cast<uint8_t>(std::lround(blue));
}

bool operator==(const Pixel & pixel1, const Pixel & pixel2)
{
  return ((pixel1.red == pixel2.red) && (pixel1.green == pixel2.green) && (pixel1.blue == pixel2.blue));
}

Pixel operator+(const Pixel & pixel1, const unsignedPixel & pixel2)
{
  int32_t red = pixel2.red + pixel1.red;
  if (red > 255)
  {
    red = 255;
  }
  else if (red < 0)
  {
    red = 0;
  }
  int32_t green = pixel2.green + pixel1.green;
  if (green > 255)
  {
    green = 255;
  }
  else if (green < 0)
  {
    green = 0;
  }
  int32_t blue = pixel2.blue + pixel1.blue;
  if (blue > 255)
  {
    blue = 255;
  }
  else if (blue < 0)
  {
    blue = 0;
  }
  return { red, green, blue };
}

unsignedPixel operator*(const double & k, const unsignedPixel & pixel1)
{
  return { static_cast<int32_t>(std::lround(k*pixel1.red)), static_cast<int32_t>(std::lround(k*pixel1.green)), static_cast<int32_t>(std::lround(k*pixel1.blue)) };
}

unsignedPixel operator+=(unsignedPixel & pixel1, const unsignedPixel & pixel2)
{
  return { pixel1.red = pixel2.red, pixel1.green = pixel2.green, pixel1.blue = pixel2.blue };
}