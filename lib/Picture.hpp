#ifndef PICTURE_HPP
#define PICTURE_HPP

#include <vector>
#include <experimental/filesystem>

#include "opencv2/core.hpp"

#include "BaseTypes.hpp"

class Picture
{
public:
  Picture(const Picture& image, const uint16_t bitCount);
  Picture(const std::experimental::filesystem::path& path);
  Picture(const uint32_t height, const uint32_t width, const uint16_t bitCount);

  void writeToFile(const std::experimental::filesystem::path& path);
  cv::Mat convertToOpenCV() const;

  uint32_t getWidth() const;
  uint32_t getHeight() const;
  uint16_t getBitCount() const;
  void setPixelAt(const uint32_t heigth, const uint32_t width, const Pixel pixel);
  Pixel getPixelAt(const uint32_t heigth, const uint32_t width) const;
  void setPalette(const std::vector <Pixel>& palette);
  void setPalette(const cv::Mat palette);
  void setSpreadMatrix(const std::vector<std::vector<double>> matrix, const uint8_t spreadMatrixStartJ);
  std::vector <Pixel> getPalette() const;
  std::experimental::filesystem::path getPath() const;

  std::vector <Pixel> createPalette(const size_t nColour, bool var);
  cv::Mat createPaletteOpenCV(const int nColour) const;
  std::vector <statsPixel> getUsedColours();

  void putPixelInData(const Pixel pixel);
  void putPixelInData(const uint16_t pixel);
  void putPixelInData(const uint8_t pixel, const uint8_t count);
  void putPixelInData(const int pixel);

private:
  BmpHeader bmpHeader_;
  std::vector <Pixel> palette_;
  std::vector<std::vector<Pixel>> bitMap_;
  std::experimental::filesystem::path pathToFile_;
  std::vector <statsPixel> usedColours_;
  std::vector <uint8_t> rawData_;

  double coefficients_red_;
  double coefficients_blue_;
  double coefficients_green_;

  std::vector<std::vector<double>> spreadMatrix_;
  uint8_t spreadMatrixStartJ_;
  void spreadError(std::vector<std::vector<unsignedPixel>>& errors, const uint32_t heigth, const uint32_t width, const Pixel existingPixel, const Pixel needPixel);

  void prepareToWrite();
  int findNearestInPalette(const Pixel pixel) const;
};

#endif