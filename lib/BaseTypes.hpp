#ifndef BASETYPES_HPP
#define BASETYPES_HPP

#include <cstdint>

struct Pixel
{
  uint8_t red;
  uint8_t green;
  uint8_t blue;

  Pixel();

  Pixel(const int red, const int green, const int blue);

  Pixel(const uint8_t red, const uint8_t green, const uint8_t blue);

  Pixel(const float red, const float green, const float blue);
};

bool operator==(const Pixel & pixel1, const Pixel & pixel2);

struct statsPixel
{
  Pixel pixel;
  uint64_t nUsage;
};

struct FileHeader
{
  uint16_t signature;
  uint32_t fileSize;
  uint32_t reserved;
  uint32_t dataOffset;
};

struct InfoHeader
{
  uint32_t infoHeaderSize;
  uint32_t width;
  uint32_t height;
  uint16_t planes;
  uint16_t bitCount;
  uint32_t compression;
  uint32_t imageSize;
  uint32_t xPixelsPerM;
  uint32_t yPixelsPerM;
  uint32_t colorsUsed;
  uint32_t colorsImportant;
};

struct BmpHeader
{
  FileHeader fileHeader;
  InfoHeader infoHeader;
};

struct unsignedPixel
{
  int32_t red;
  int32_t green;
  int32_t blue;
};

Pixel operator+(const Pixel & pixel1, const unsignedPixel & pixel2);

unsignedPixel operator*(const double & k, const unsignedPixel & pixel1);

unsignedPixel operator+=(unsignedPixel & pixel1, const unsignedPixel & pixel2);

#endif