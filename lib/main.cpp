#include <experimental/filesystem>
#include <ctime>

#include "Picture.hpp"

int main()
{
  Picture input("1.bmp");

  Picture output(input, 8);
  output.setPalette(input.createPalette(256, true));
  
  //unsigned int t1 = clock();
  output.writeToFile("2.bmp");
  //unsigned int t2 = clock() - t1;
  //std::unordered_map<Pixel, int, PixelHash, PixelEqual> usedColours_ = input.getUsedColours();
  //input.writeToFile("C:/Users/Nikita/Desktop/BMP/Image/2_8_.bmp");
  return 0;
}
